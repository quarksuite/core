# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Table of Contents

- [v2.2.0 - 2022-08-02](#v2-2-0-2022-08-02)
- [v2.1.3 - 2022-07-12](#v2-1-2-2022-07-12)
- [v2.1.2 - 2022-07-09](#v2-1-2-2022-07-09)
- [v2.1.1 - 2022-07-05](#v2-1-1-2022-07-05)
- [v2.1.0 - 2022-07-04](#v2-1-0-2022-07-04)
- [v2.0.2 - 2022-06-18](#v2-0-2-2022-06-18)
- [v2.0.1 - 2022-06-17](#v2-0-1-2022-06-17)
- [v2.0.0 - 2022-06-17](#v2-0-0-2022-06-17)
- [v1.3.0 - 2022-02-25](#v1-3-0-2022-02-25)
- [v1.2.1 - 2022-02-24](#v1-2-1-2022-02-24)
- [v1.2.0 - 2022-02-21](#v1-2-0-2022-02-21)
- [v1.1.0 - 2022-02-16](#v1-1-0-2022-02-16)
- [v1.0.1 - 2021-12-17](#v1-0-1-2021-12-17)
- [v1.0.0 - 2021-12-16](#v1-0-0-2021-12-16)
- [v0.2.1 - 2021-06-07](#v0-2-1-2021-06-07)
- [v0.2.0 - 2021-05-27](#v0-2-0-2021-05-27)
- [v0.1.1 - 2021-04-29](#v0-1-1-2021-04-29)
- [v0.1.0 - 2021-04-13](#v0-1-0-2021-04-13)

## v2.2.0 - 2022-08-02

This minor release properly clamps RGB values during calculation. Prior to this, conversions between some formats would
sometimes return as black or white because an unclamped RGB channel swung the calculation way off target.

## v2.1.3 - 2022-07-12

This patch fixes the documentation links and references. They weren't entirely correct after the move to Codeberg.

## v2.1.2 - 2022-07-09

This patch fixes a bug where the output of scales set with a multithreaded ratio wasn't sorting the values correctly. This
was caused by way too early serialization of output before the values were properly sorted.

## v2.1.1 - 2022-07-05

This patch marks the QuarkSuite project's move from GitHub to Codeberg. In addition, I've changed all documentation from
Org files to more familiar and standard GFM (GitHub Flavored Markdown). This should make it a bit easier for people to
read the documentation and contribute.

## v2.1.0 - 2022-07-04

This minor version moves palette accessibility and color perception simulation out of `palette()` as separate palette
actions. This makes them explicit operations to run *after* creating a palette collection.

```js
import {
  palette,
  a11y,
  perception
} from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.1.0/color.js";

const main = palette({ configuration: "material", accents: true }, "dodgerblue");

const ui = a11y({ mode: "standard", rating: "AA", large: true }, main);
const text = a11y({ mode: "standard", rating: "AA" }, main);

const checkUiProtanopia = perception({ check: "vision", as: "protanopia" }, ui);
```

See the updated handbook and [API documentation](https://codeberg.org/quarksuite/core/src/branch/main/API.md).

### Added

-   `a11y(settings, palette)`
-   `perception(settings, palette)`

### Changed

`palette()` action now exclusively handles palette generation. Color accessibility and perception simulators are handled
by the above new palette actions.

## v2.0.2 - 2022-06-18

### Fixed

-   CIELCH color parsing didn't *actually* convert the hue to radians, so CIELCH colors would shift their hue when converting to themselves.

## v2.0.1 - 2022-06-17

### Fixed

-   The `severity` setting was inactive for illuminant simulation

## v2.0.0 - 2022-06-17

QuarkSuite Core v2 is another complete rewrite of the library architecture that focuses on simplifying and tightening
the workflow introduced in v1. It also includes a significant performance boost and improves the developer experience.

For example:

```js
const color = convert("rgb", "dodgerblue");

const paletteOpts = { configuration: "material", accents: true };
const a11yOpts = { mode: "standard", rating: "AA" };

const font = "Work Sans";

const ratio = 1.618;
const values = 4;

const tokens = {
  color: {
    ui: palette({ ...paletteOpts, a11y: { ...a11yOpts, large: true } }, color),
    text: palette({ ...paletteOpts, a11y: a11yOpts }, color)
  },
  text: {
    body: text({ system: "sans", weights: ["regular", "bold" ]}, font),
    heading: text({ system: "serif", weights: ["light", "black"]}, font),
    size: scale({ configuration: "bidirectional", ratio, values }, "1rem"),
    measure: scale({ configuration: "ranged", floor: 45, trunc: true, ratio, values }, "75ch"),
    leading: scale({ configuration: "ranged", floor: 1.25, ratio, values }, 1.5)
  },
  spacing: scale({ configuration: "bidirectional", ratio, values }, "1ex"),
  grid: grid({ rows: 3, ratio }, 5),
  lengths: {
    width: scale({ configuration: "ranged", floor: 10, ratio, values }, "100vw"),
    height: scale({ configuration: "ranged", floor: 10, ratio, values }, "100vh"),
    shortest: scale({ configuration: "ranged", floor: 10, ratio, values }, "100vmin"),
    longest: scale({ configuration: "ranged", floor: 10, ratio, values }, "100vmax"),
  }
};
```

You can [read the new handbook](https://codeberg.org/quarksuite/core/src/branch/main/HANDBOOK.org) for a practical
introduction and then [dive into the API](https://codeberg.org/quarksuite/core/src/branch/main/API.org) for the
technical details.

### Added

#### simplified workflow

Core v2 reworks the utilities -> configurations -> formulas -> bootstrappers layered workflow into a more compact yet
powerful one based around actions and emitters.

#### independent modules

Core v2 still prioritizes portability but goes about it by exposing independent, complete modules instead of a single
entry point.

-   `color.js`: handles all color and palette generation actions
-   `content.js`: handles all content and modular scale generation actions
-   `exporter.js`: handles all token exporting actions
-   `workflow.js`: handles all advanced functionality

#### tightened API

The API is drilled down to 12 actions and 5 workflow helpers:

-   `color.js`
    -   `convert(to, color)`
    -   `adjust(settings, color)`
    -   `mix(settings, color)`
    -   `harmony(settings, color)`
    -   `palette(settings, color)`
    -   `output(format, dict)`
-   `content.js`
    -   `text(settings, font)`
    -   `grid(settings, columns)`
    -   `scale(settings, root)`
-   `exporter.js`
    -   `stylesheet(format, dict)`
    -   `data(format, dict)`
    -   `schema(settings, root)`

-   `workflow.js`
    -   `preset(action, y)`
    -   `process(...emitters)`
    -   `pipeline(x, ...emitters)`
    -   `propagate(emitter, xs)`
    -   `delegate(xs, ...emitters)`

#### faster performance

Core v2 brings significant performance improvements. The benchmarks against v1 are simply incomparable. Color and
palette generation in particular is now an **order of magnitude** faster than the last stable version.

Here's the run of benchmarks right before launch.

```shell
running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/color/adjust_bench.js
bench adjust_stress ... 100 iterations 31,059,348 ns/iter (27,543,853..62,799,329 ns/iter) ok (3s)

running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/color/convert_bench.js
bench convert_stress ... 100 iterations 3,716,583 ns/iter (3,013,710..7,516,550 ns/iter) ok (469ms)

running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/color/harmony_bench.js
bench harmony_stress ... 100 iterations 667,102,923 ns/iter (657,087,747..697,010,684 ns/iter) ok (1m10s)

running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/color/mix_bench.js
bench mix_stress ... 100 iterations 30,014,753 ns/iter (26,078,073..55,965,712 ns/iter) ok (3s)

running 2 benches from file:///home/cr-jr/Code/quarksuite:core/benchmarks/color/output_bench.js
bench gpl_stress ... 100 iterations 7,152,389 ns/iter (5,780,415..23,781,011 ns/iter) ok (842ms)
bench sketchpalette_stress ... 100 iterations 8,240,688 ns/iter (7,713,674..14,122,728 ns/iter) ok (904ms)

running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/color/palette_bench.js
bench palette_stress ... 100 iterations 119,840,106 ns/iter (114,942,695..137,234,079 ns/iter) ok (13s)

running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/content/grid_bench.js
bench grid_stress ... 100 iterations 112,581,195 ns/iter (111,258,202..119,756,184 ns/iter) ok (11s)

running 1 bench from file:///home/cr-jr/Code/quarksuite:core/benchmarks/content/scale_bench.js
bench scale_stress ... 100 iterations 16,586,855 ns/iter (15,783,908..19,464,515 ns/iter) ok (1s)

running 2 benches from file:///home/cr-jr/Code/quarksuite:core/benchmarks/exporter/data_bench.js
bench json_stress ... 100 iterations 1,573,172 ns/iter (1,421,585..2,103,778 ns/iter) ok (176ms)
bench yaml_stress ... 100 iterations 5,096,098 ns/iter (4,015,251..9,644,907 ns/iter) ok (555ms)

running 2 benches from file:///home/cr-jr/Code/quarksuite:core/benchmarks/exporter/schema_bench.js
bench tailwindcss_stress ... 100 iterations 340,207,906 ns/iter (338,373,124..351,732,590 ns/iter) ok (35s)
bench style_dictionary_stress ... 100 iterations 342,807,761 ns/iter (340,656,522..356,422,199 ns/iter) ok (36s)

running 4 benches from file:///home/cr-jr/Code/quarksuite:core/benchmarks/exporter/stylesheet_bench.js
bench css_stress ... 100 iterations 8,507,013 ns/iter (7,272,616..13,305,865 ns/iter) ok (963ms)
bench scss_stress ... 100 iterations 8,034,568 ns/iter (7,224,880..11,457,692 ns/iter) ok (853ms)
bench less_stress ... 100 iterations 8,134,869 ns/iter (7,228,801..12,678,502 ns/iter) ok (865ms)
bench styl_stress ... 100 iterations 7,561,211 ns/iter (7,084,911..10,677,659 ns/iter) ok (804ms)

bench result: ok. 17 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (3m5s)
```

And this is on a PC with a *Celeron* processor and 3GB of RAM. It'll surely be even faster on many developers' machines.

#### color accessibility enhancements

Core v2 adds refinements to checking and filtering your palettes for accessibility.

-   perception simulators
    -   check various forms of colorblindness
    -   check contrast sensitivity
    -   check illuminants (light sources)
-   custom colorimetric contrast tuning in addition with WCAG accessibility standards

#### improved developer experience

Core v2 includes important quality-of-life updates to make it more straightforward and enjoyable to use.

-   properly documented types and generated for all modules
-   updated NPM package
-   support for JSDelivr and Statically CDNs in addition to the Nest.land package
-   less boilerplate when generating data

## v1.3.0 - 2022-02-25

This minor release updates the implementation of OKLab and OKLCH to [adhere to the currently defined
spec](https://www.w3.org/TR/css-color-4/#specifying-oklab-oklch).

This means that Quarks System Core now supports the use of colors in most major current and emerging formats.

Specifically:

-   Named colors
-   RGB Hex
-   Functional RGB
-   Functional HSL
-   Functional device-cmyk
-   Functional HWB
-   Functional CIELAB
-   Functional CIELCH
-   Functional OKLab
-   Functional OKLCH

### Added

-   `color_to_oklab`

### Changed

-   renamed `color_to_oklab` to `color_to_oklch`
-   `color_to_oklab` output takes over `oklab` prefix
-   OKLCH color prefix now `oklch`

## v1.2.1 - 2022-02-24

Fixes `color_adjust` and `color_filter` and improves chromatic accuracy for color generation in general.

### Fixed

-   chromatic adjustment through the OKLab color space was *not* bound to a range 0-0.5. This threw off chromatic
    accuracy and made chroma impossible to filter for

## v1.2.0 - 2022-02-21

This release updates the *object factory* workflow with more consistent names and better propagation over values and
scales.

-   `$_` will cycle the execution over values (`object.$_adjust()`)
-   `$$_` will cycle the execution over scales (`object.$$_modify()`)

This change allows you even more control over your generated data.

In addition, the setup has changed:

```js
import * as Q from "https://x.nest.land/quarksuite:core@1.2.0/mod.js";

const { fn_compose, fn_curry fn_filter, fn_to_factory } = Q;

// Initialize a factory: fn_filter -> [fn] -> fn_to_factory -> (x) => object
const Color = fn_compose(fn_curry(fn_filter, "color") fn_to_factory);

// Create an instance
const swatch = Color("lime");

// Instance methods are encapsulated in its prototype, so you won't see the data
// until you invoke one
console.log(swatch) // {}
console.log(swatch.to_hex()) // { x: "#00ff00" }

// do stuff with it (now only strips the type in translation)
swatch.to_rgb();
swatch.to_scheme_triadic();

// factory methods are shadowed by $_ and $$_ equivalents that transform data recursively.
// $_ iterates over each value recursively and executes, $$_ iterates over scales of values and executes
// e.g.: $_material means ([a, b, c] -> [[50..900], [50..900], [50..900]])
swatch.to_rgb().to_scheme_triadic().$_material();

// Every instance has a data getter that extracts the result
const { data: palette } = swatch.to_rgb().to_scheme_triadic().$_material();

// Which can then be the initializer for another instance
const { data: secondaryPalette } = Color(swatch).$_adjust({ hue: 120 });
```

### Changed

-   `imports_to_module` renamed to `fn_filter`
-   `module_to_factory` renamed to `fn_to_factory`

### Fixed

-   errors with data propagation when the method expected a scale (now handled by `$$_` methods)

## v1.1.0 - 2022-02-16

This release adds support for a factory object workflow. It makes working with low level utilities a little easier by
allowing you to bind them as methods on discrete data types. You can set up this workflow with a few extra lines of
code:

```js
import * as qsc from "https://x.nest.land/quarksuite:core@1.1.0/mod.js";

const { imports_to_module, module_to_factory } = qsc;

// Initialize the factory
const Color = module_to_factory(imports_to_module("color", qsc));

// Create an instance
const swatch = Color("lime");

// do stuff with it

swatch.rgb();
swatch.rgb().triadic()

// factory methods are shadowed by $-prefixed equivalents that transform data recursively.
// e.g.: $material means ([a, b, c] -> [[50..900], [50..900], [50..900]])
swatch.rgb().triadic().$material();
```

### Added

Factory interface utilities

-   `imports_to_module(type, import)`
-   `module_to_factory(module)`

### Changed

-   `utility` type renamed to `fn` (`fn_compose`, `fn_curry`, `fn_pipe`) as they're not exclusively used by utilities
-   `color_to_scheme_split_complementary` renamed to `color_to_scheme_split`
-   `tokens_to_style_dictionary` renamed to `tokens_to_styledict`

## v1.0.1 - 2021-12-17

### Added

-   Examples for `utility_compose`, `utility_curry`, `utility_pipe`

### Fixed

-   Malformed TSDoc typing
-   Function documentation typos

## v1.0.0 - 2021-12-16

Quarks System Core v1 is here and it's another major rewrite. To begin, the workflow is a completely different
beast. The naming conventions are more deliberate, the design token spec is more or less settled, and I've reinforced
the library's web focus.

### Added

#### revamped workflow

-   Basic: automatic token generation via bootstrapper
-   Intermediate: manual token generation via formulas
-   Advanced: granular token generation via utilities

#### explicit naming conventions

-   `PascalCase` for bootstrapper/formulas
-   `snake_case` for utilities/exporters
-   conventional `camelCase` for internal functionality

#### overhauled API

-   too much to list, recommend [reading it](https://observablehq.com/@cr-jr/qsc-api)

## v0.2.1 - 2021-06-07

### Fixed

#### color adjustment

-   bug where the presence of alpha component was implemented in standard LCh(ab) instead of Oklab, which rendered the
    color adjustment achromatic

## v0.2.0 - 2021-05-27


<a id="orgf46a703"></a>

### Added

#### color conversion

-   Added *non-standard* Oklab (LCh) color format support

### Changed

#### color adjustment

-   All color adjustment functions are now implemented through the Oklab color space for better hue linearity and
    lightness/chroma predictions
-   Adjustments are perceptually uniform and blue hues no longer shift toward purple

#### color mixture

-   Color mixing now uses simpler interpolation formula through the Oklab color space

#### color schemes

-   All basic color schemes have their output explicitly slotted into a fixed scale. This means `custom()` is the only
    true dyanamic color scheme generator. Use it carefully

#### ~~color palette~~ color scale

-   `color_palette.js` module renamed to `color_scale.js` (which is a better description of its purpose)
-   New interpolation formula makes `contrast` parameter slightly more sensitive (particularly with shades)

## v0.1.1 - 2021-04-29

### Changed

#### color conversion

-   refactored conversion logic to be simpler

## v0.1.0 - 2021-04-13

The initial release of Quarks System Core adds several improvements over QuarkSuite 1. For a full overview of what's
different from legacy, consult the [Changelog of QuarkSuite
1](https://github.com/quarksuite/legacy/blob/master/CHANGELOG.md#v500---2020-12-05).

This project no longer relies on Node or NPM and is instead served directly from the Arweave permaweb via
[nest.land](https://nest.land). Use `https://x.nest.land/quarksuite:core/mod.js` in a browser console or in your scripts
to get started.

TypeScript is also gone to remove the compile step that would conflict with using the library in its target
environments. Basic type information is provided for development via TSDoc comments.

Top level source files are now modules by default, and the below modules should be preferred for importing:

-   `mod.js`: the entry point for the entire public API (aggregates the following)
    -   `utilities.js`: contains advanced functional utilities
    -   `color.js`: contains all color functions
    -   `typography.js`: contains all typography functions
    -   `modular_scale.js`: contains all functions for using modular scales
    -   `design_tokens.js`: contains all build functions

Lastly, I've used literate programming through the development of Quarks System Core to clarify the implementation
details, so you'll find [the documentation is also the
source](https://codeberg.org/quarksuite/core/src/branch/main/README.org#source-code).

### Added

#### functional utilities

-   `compose` function for straightforward composition

#### color conversion

-   `device-cmyk`, `hwb`, `lab`, `lch` CSS formats now valid

#### scheme functions

-   simplified `analogous`, `splitComplementary`, `triadic`, `tetradic/dualComplementary`, `square` functions provided
    for basic schemes; no longer need to set modifiers

#### color accessibility

-   `contrast` function for validating or filtering palettes with WCAG color contrast ratios recommendations

### Changed

#### scheme functions

-   output of basic schemes slightly rearranged
-   `custom` color scheme generation modified to create colors around the input color symmetrically

#### output functions

-   `yaml` data export added
-   `tw` changed to `tailwind`
-   `sd` changed to `styledict`

### Removed

#### functional utilities

-   `bind` scrapped in favor of JavaScript's native `Function.bind`
