# QuarkSuite Core (v2.2.0)

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Packages](#packages)
- [Getting Started](#getting-started)
  - [Live](#live)
  - [Local Development](#local-development)
  - [Offline](#offline)
- [Handbook](#handbook)
- [API](#api)
- [Demos](#demos)
- [Contributing](#contributing)
- [Concept](#concept)
- [Project Objectives](#project-objectives)
  - [Availability](#availability)
  - [Adaptability](#adaptability)
  - [Invisibility](#invisibility)
  - [Interoperability](#interoperability)
  - [Stability](#stability)
- [Credit](#credit)

## Introduction

QuarkSuite Core is the engine of the [emerging QuarkSuite toolkit][quarksuite] which is focused around using the web
platform as a [design token][design-tokens] development environment.

The core in particular is responsible for generating, assembling, and exporting token collections and dictionaries.

QuarkSuite Core is written in vanilla JavaScript (ES2017+).

## Features

-   Generate practical color palettes for apps and sites
    -   Color adjustment and blending
    -   Built-in colorblindness, contrast sensitivity, and illuminant simulations
    -   Material-esque and artistic configurations
    -   WCAG-based and colorimetic color accessibility filtering
-   Enforce compositional balance with modular scales
-   Export tokens to take with you
    -   Graphic design palettes (GIMP/Inkscape, Sketch)
    -   Stylesheet formats (CSS custom properties, Sass, Less, Stylus)
    -   Data formats (JSON, YAML)
-   Interop with [TailwindCSS][tailwindcss] and [Style Dictionary][style-dictionary]
-   Scales with your needs
    -   from basic generation and assembly
    -   to advanced rules and processes
    -   to modular configurations
-   Independent modules
    -   `color.js` for color features
    -   `content.js` for content features
    -   `exporter.js` for exporting features
    -   `workflow.js` for advanced features

## Packages

-   [Node](https://nodejs.org/) (NPM): `@quarksuite-two/core@2.2.0`
-   [JSDelivr](https://jsdelivr.com): `https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/[module]`
-   [Skypack](https://skypack.dev): `https://cdn.skypack.dev/@quarksuite-two/core@2.2.0/[module]`
-   [esm.sh](https://esm.sh): `https://esm.sh/@quarksuite-two/core@2.2.0/[module]`
-   [Nest.land](https://nest.land): `https://x.nest.land/quarksuite:core@2.2.0/[module]`

In general, prefer the JSDelivr CDN for availability, features, and speed. The Nest.land package is a failsafe for NPM
going down.

## Getting Started

### Live

If you want to try the library live, load up your console in a modern browser or run a `deno repl` and copy/paste:

```js
const {
  color,
  content,
  exporter,
  workflow
} = await import("https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/mod.js");
```

### Local Development

For local dev, it's recommended that you [use a buildless stack](https://buildless.site/) or work in the
browser. QuarkSuite Core is designed especially for that.


#### Browser/Deno

```js
import {
  color,
  content,
  exporter,
  workflow
} from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/mod.js";
```

#### Node

> The now defunct `@quarksuite/core` was originally published on NPM, but I started the project over and unpublished the
> package. The core is now in a state where I feel comfortable putting it back on NPM and exposing it to the wider
> JavaScript ecosystem.

Ensure that `type: "module"` is set in your `package.json` so that Node is set for ES Modules. Then install the package.

```shell
npm install @quarksuite-two/core@2.2.0

# OR

yarn add @quarksuite-two/core@2.2.0
```

And then in your source files:

```js
import {
  color,
  content,
  exporter,
  workflow
} from "@quarksuite-two/core";
```

### Offline

Some engines (like QuickJS) support ES Modules but don't allow importing from URLs. For those, you can `curl/wget` the
modules you need directly.

```shell
curl -o [module] https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/[module]

# OR

wget https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/[module]
```

## Handbook

The [QuarkSuite Core Handbook][qsc-handbook] introduces the features of the library from a user perspective. It's
recommended you read that next.

## API

The [QuarkSuite Core API][qsc-api] is a technical overview of the library that also serves as documentation of its
functions. It may make more sense after reading the handbook, but some of you may get more out of reading it beforehand.

## Demos

I maintain a [growing collection of demos][qs-demos] for QuarkSuite at various stages of development on Glitch. It
includes rough experiments, workflow samples, and practical usage examples, so be sure to check it out.

## Contributing

First, clone the repo to your local machine.

```shell
git clone https://codeberg.org/quarksuite/core.git
```

Then make sure you [read the contribution guidelines][qsc-contributing].

## Concept

QuarkSuite is a project built around the idea that authoring design tokens shouldn't be limited to a particular
environment or web stack. Many design tools build entire systems around themselves.

I wanted a workflow where I could open a blank page in a browser and start experimenting. I wanted tools with mobility
across environments. I wanted something that could grow seamlessly with my needs and allow me to automate my
requirements.

QuarkSuite aims to be that toolkit.

## Project Objectives

The following objectives guide the ongoing development of QuarkSuite.

### Availability

This project focuses on the overall availability of web technologies over the popularity of particular web stacks. Its
independent modules, relatively small size, and simple output all work to make the kit extremely portable.

### Adaptability

QuarkSuite Core aims to grow or shrink according to your needs. Its functional nature makes it straightforward to use
and extend as you see fit.

### Invisibility

This project is a means to an end: creating design tokens. I don't want its processes to call attention to themselves or
its design to get in your way. Use what you need and leave the rest.

### Interoperability

QuarkSuite Core is built to work with what already exists in the design token and UI theming space. It can be used on
its own, but its schema is nothing but a plain object.

This makes it particularly suited for integration with design tools that use their *own* schemas (such as
[TailwindCSS][tailwindcss] and [Style Dictionary][style-dictionary]).

### Stability

Where applicable, this project aims to adhere as closely as possible to current and emerging web standards. From its use
of standard CSS color formats to its avoidance of bleeding-edge syntax.

## Credit

-   Color conversion math and formulas
    -   hex, rgb, hsl: [RapidTables](https://www.rapidtables.com/convert/color/index.html)
    -   cmyk, hwb: [W3C CSS Color Module 4](https://drafts.csswg.org/css-color-4/) (though, as of this writing,
        `device-cmyk()` is bumped to Color Module 5)
    -   lab, lch: [Bruce Lindbloom](http://www.brucelindbloom.com/index.html?Math.html)
    -   oklab, oklch: [Björn Ottosson](https://bottosson.github.io/posts/oklab/) (the creator of the Oklab color space)
-   Color perception simulators
    -   color vision deficiency: [libDaltonLens](https://github.com/DaltonLens/libDaltonLens)
    -   correlated color temperature (CCT): [temperargb](https://github.com/m-lima/temperagb)
-   Built-in datasets:
    -   Named colors: [W3C Wiki](https://www.w3.org/wiki/CSS/Properties/color/keywords)
    -   System font stacks: [System Font Stack](https://systemfontstack.com)
-   design token schema: [Styled System](https://styled-system.com/theme-specification/) and [Style
    Dictionary](https://amzn.github.io/style-dictionary/#/tokens)

[quarksuite]: https://codeberg.org/quarksuite
[qsc-handbook]: https://codeberg.org/quarksuite/core/src/branch/main/HANDBOOK.md
[qsc-api]: https://codeberg.org/quarksuite/core/src/branch/main/API.md
[qsc-contributing]: https://codeberg.org/quarksuite/core/src/branch/main/CONTRIBUTING.md
[qs-demos]: https://glitch.com/@quarksuite

[design-tokens]: https://css-tricks.com/what-are-design-tokens/

[tailwindcss]: https://tailwindcss.com
[style-dictionary]: https://amzn.github.io/style-dictionary/
